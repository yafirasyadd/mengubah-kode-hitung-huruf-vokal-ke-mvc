package com.cekvokal.yafirasyadmvccekvokal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YafirasyadMvcCekvokalApplication {

	public static void main(String[] args) {
		SpringApplication.run(YafirasyadMvcCekvokalApplication.class, args);
	}

}
