package com.cekvokal.yafirasyadmvccekvokal.controller;

import com.cekvokal.yafirasyadmvccekvokal.model.Vokal;
import com.cekvokal.yafirasyadmvccekvokal.service.VokalService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class VokalController {

    private VokalService vokalService;

    @RequestMapping("")
    public String countVowel(Vokal vokal, Model model) {
        String str = vokal.getStr();
        String count = vokal.getCount();
        String kar = vokal.getC();

        if (count != null) {
            count += vokalService.countStr(str);
            model.addAttribute("str", str);
            model.addAttribute("count", count);
        }

        //belum berhasil
//        if (kar != null) {
//            kar += vokalService.karakter(kar);
//            model.addAttribute("karakter", kar);
//
//        }


        return "index";
    }
}

