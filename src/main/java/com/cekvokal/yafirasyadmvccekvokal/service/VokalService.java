package com.cekvokal.yafirasyadmvccekvokal.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class VokalService {



    public static int countStr(String str) {
        Set<Character> set = new HashSet<Character>();
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (isVowel(c)) {
                set.add(c);
            }
            count = set.size();
        }

        return count;

    }
// belum berhasil
//    public static String karakter(String kar) {
//        Set<Character> set = new HashSet<Character>();
//
//        for (int i = 0; i < kar.length(); i++) {
//            char c = kar.charAt(i);
//            if (isVowel(c)) {
//                set.add(c);
//            }
//        }
//            return set.toString();
//    }
    public static boolean isVowel(char character)
    {

        if(character=='a' || character=='A' || character=='e' || character=='E' ||
                character=='i' || character=='I' || character=='o' || character=='O' ||
                character=='u' || character=='U'){
            return true;
        }else{
            return false;
        }
    }

}

